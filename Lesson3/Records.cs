﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Xml;

namespace Lesson3
{
    public class Records : IEnumerable<Record>
    {
        private RecordsAlgorithm _algorithm;
        private Record[] _records;
        public Records(XmlNode nodes)
        {
            _algorithm = new RecordsAlgorithm();
            _records = new Record[nodes.ChildNodes.Count];
            for (int i = 0; i < _records.Length; i++)
            {
                _records[i] = new Record(nodes.ChildNodes[i]);
            }
        }

        public IEnumerator<Record> GetEnumerator()
        {
            return new RecordsEnumerator(_records);
        }


        // А если я так сделаю
        public IEnumerable<Record> Sort()
        {
            return _algorithm.Sort(_records);
        }
        public IEnumerable<Record> Search(string phone=null, string name=null)
        {
            if (!string.IsNullOrEmpty(phone))
                return _algorithm.Search(_records, phone, null);
            else if (!string.IsNullOrEmpty(name))
            {
                return _algorithm.Search(_records, null, name);
            }
            return null;
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
