﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson3
{
    public interface IAlgorithm
    {
        IEnumerable<Record> Sort(IEnumerable<Record> records);
        IEnumerable<Record> Search(IEnumerable<Record> records, string firstName = null, string phone = null);
    }
}
