﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lesson3
{
    public class RecordsAlgorithm : IAlgorithm
    {
        public IEnumerable<Record> Search(IEnumerable<Record> rec, string firstName = null, string phone = null)
        {
            if (phone != null)
            {
                var records = rec.Where(record => record.Phone == phone);
                return records;
            }
            else
            {
                var records = rec.Where(record => record.FirstName == firstName);
                return records;
            }
        }

        public IEnumerable<Record> Sort(IEnumerable<Record> records)
        {
            var recordsArr = records as Record[];
            for (int i = 0; i < recordsArr.Length; i++)
            {
                for (int j = 0; j < recordsArr.Length - 1 - i; j++)
                {
                    if (recordsArr[j].Date.CompareTo(recordsArr[j + 1].Date) == 1)
                    {
                        Swap(j, j + 1, ref recordsArr);
                    }
                }
            }
            return records;
        }
        private void Swap(int i, int j, ref Record[] records)
        {
            if (i < records.Length && j < records.Length)
            {
                var temp = records[i];
                records[i] = records[j];
                records[j] = temp;
            }
        }
    }
}
